#define USE_CONSOLE
#include <allegro.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include "menu.h"
#include "inicia.h"
#define random(n) rand()%(n) // Emula funcion de Borland (Num aleat 0..n-1)
const int TAMX = 1000;
const int TAMY = 700;
const int VENTX = 550;
const int VENTY = 750;
class Bolita{

public:
   // BITMAP *buffer = create_bitmap(TAMX,TAMY);
    bool seguir;
   int x , y;
   int dx,dy;
   int radio;
   int color;
   void asigcoord (int a , int b,int r,int c) { x = a; y = b; radio = r; dx = 4 ; dy = 4;color=c;}
   void pintar(int xn,int yn){circlefill(screen,xn,yn,radio,color);}
    void pintar(){circlefill(screen,x,y,radio,color);}
    void pintar(int color1){circlefill(screen,x,y,radio,color1);}
    void borrar(){circlefill(screen,x,y,radio,makecol(0, 500, 150));}
   void mover ();
   void mover2 ();
   void mover3 ();
   void set_color(int x){
   color= x;
   }
   Bolita(){
    //clear_to_color(buffer,283);
   seguir=true;
   };

};
void Bolita::mover(){

   borrar();
 x+=dx; y+=dy;
if( x > TAMX - radio || x < radio){dx = -dx;}
if( y > TAMY - radio +5 || y < radio){ dy = -dy;}

pintar();



}


bool circle_overlaps_precision(const int x1, const int y1, const int r1,
                              const int x2, const int y2, const int r2) {

    return (r1+r2 < sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2)));
}
bool CoqPel(Bolita &A , Bolita &B,int vidas);
void CoqPel(Bolita &A , Bolita &B){
      if(circle_overlaps_precision(A.x,A.y,A.radio,B.x,B.y,B.radio)){
            A.dx = -A.dx;
            B.dx = -B.dx;
            A.dy = -A.dy;
            B.dy = -B.dy;
    }
}

int main()
{
    int c=1000, hilo=0, ultimohilo=7,tiempo=0,temp=0;
    bool salida=false;
    srand(time(NULL));
    DIALOG *d;

BITMAP * buffer;
    inicia_allegro(TAMX,TAMY);
    inicia_audio(70,70);
    install_mouse();
    install_timer();
    install_keyboard();
    set_window_title("SPACE CIRCULE- TEJE");
   // object_message(d,400,500);
    MSG_START;
    MSG_DRAW;

 // foo(MSG_START, d, 283);
 //allegro_message("Gracias por elegir este juego");
 //  BITMAP *buffer = create_bitmap(TAMX,TAMY);
    //BITMAP *fondo1 = load_bitmap("FONDO1.bmp",NULL);
    //BITMAP *fondo2 = load_bitmap("FONDO2.bmp",NULL);
   // BITMAP *fondo3 = load_bitmap("FONDO3.bmp",NULL);
   // BITMAP *cursor = load_bitmap("cursor.bmp",NULL);

    /*while(!salida){
        if(mouse_x > 211 && mouse_x < 553 &&
           mouse_y > 319 && mouse_y < 374){

        blit(fondo2,buffer,0,0,0,0,800,530);
        if(mouse_b && 1){
            salida = true;
        }
           }else if(mouse_x > 211 && mouse_x < 404 &&
                    mouse_y > 418 && mouse_y < 465)
                    {
                        blit(fondo3,buffer,0,0,0,0,800,530);
                        if(mouse_b && 1){
                            salida = true;
                            exit(5);
                            }
                    }else blit(fondo1,buffer,0,0,0,0,800,530);

                    masked_blit(cursor,buffer,0,0,mouse_x,mouse_y,13,22);
                    blit(buffer,screen,0,0,0,0,800,530);

    }
*/
   // destroy_bitmap(buffer);
   // clear_to_color(screen,83);

 set_color_depth(32);
set_gfx_mode(GFX_AUTODETECT_WINDOWED, TAMX, TAMY, 0, 0);
Bolita A,B,C,D,E,F;
   //int x = 300, y = 300, vel = 5;
  // set_clip(screen, 0, 0, 800, 600);
  buffer = create_bitmap(800, 600);
clear_to_color(screen,makecol(0, 500, 150));
A.asigcoord(100,500,15, makecol(60,150, 350));
B.asigcoord(100,403,5+rand()%25, makecol(20, 350, 130));
C.asigcoord(80,100,5+rand()%20, makecol(50, 40, 80));
E.asigcoord(40+rand()%100,60+rand()%255,5+rand()%25, makecol(250, 30, 150));
F.asigcoord(400,300,5+rand()%20, makecol(250, 120, 50));
D.asigcoord((TAMX/2),(TAMY/2),18, makecol(250, 500, 150));
text_mode(-1);

show_mouse(screen);
 //set_mouse_sprite(cursor);
//textout(screen,font,"ganaste",600,400,213);
    while(!key[KEY_ESC]){

hilo=(hilo==ultimohilo?1:hilo+1); // Contador circular
switch(hilo) {
     case 1:
      if(!CoqPel(D,A,5)){
      A.mover3();
      }
      else{
    clear_to_color(screen,makecol(0, 500, 150));
   A.asigcoord(100,500,15, makecol(60,150, 350));
    A.dx=-A.dy;
    A.dy=-A.dx;
     // A.borrar();

      }
      rest(25);break;
    case 2:

if(D.radio>50){
    D.radio=40;
}
D.pintar( makecol(250, 50, 50));

        if(mouse_x > (D.x+D.radio+10)){
                D.borrar();
                D.x += 10;
if(D.x>(TAMX-D.radio%2)){
            D.x=(D.radio%2);
        }
        D.pintar( makecol(250, 50, 50));
        }
        if(mouse_x < (D.x-D.radio-10)){
                D.borrar();
               D.x -= 10;
        if(D.x<(D.radio%2)){
            D.x=TAMX-D.radio;
        }
        D.pintar( makecol(250, 50, 50));
        }
        if(mouse_y < (D.y-D.radio-10)){
                D.borrar();
                D.y -= 10;
        if(D.y<(D.radio%2)){
            D.y=TAMY-(D.radio%2);
        }
        D.pintar( makecol(250, 50, 50));
}
        if(mouse_y > (D.y+D.radio+10)){
            D.borrar();
            D.y += 10;
        if(D.y>(TAMY+D.radio%2)){
            D.y=D.radio%2;
        }
        D.pintar( makecol(250, 50, 50));
        }
                break;
                case 3:
      CoqPel(A,B);
      CoqPel(B,C);
      CoqPel(A,C);
    /*  CoqPel(A,E);
      CoqPel(E,B);
      CoqPel(E,C);*/
                    break;
                case 4:
    if(!CoqPel(D,B,5)){
      B.mover3();
      }
      else{
    clear_to_color(screen,makecol(0, 500, 150));
   B.asigcoord(100,500,15, makecol(60,150, 350));
  //  B.dx=-A.dy;
   // B.dy=-A.dx;
     B.pintar();
      }
                    break;
                        case 5:
      if(tiempo>1000){

      if(!CoqPel(D,C,5)){
      C.mover2();
      }
      else{
     clear_to_color(screen,makecol(0, 500, 150));
   C.asigcoord(100,500,15, makecol(60,150, 350));
   // C.dx=-A.dy;
   // C.dy=-A.dx;
    C.pintar();
    ;}
      }
      else{
       // C.borrar();
      }
  break;
                        case 6:

         if(!CoqPel(D,E,5)){
      E.mover();
      }
      else{
    E.dx=-E.dy;
    E.dy=-E.dx;
    ;}

            break;

        }
        tiempo++;
c--;
}

 return 0;

    }

END_OF_MAIN();

bool CoqPel(Bolita &A , Bolita &B,int vidas){

   if(!circle_overlaps_precision(A.x,A.y,A.radio,B.x,B.y,B.radio)){
     if(A.radio>=B.radio and B.seguir){
        A.radio+=10;
        B.dx=-B.dy;
        B.dy=-B.dx;
        B.seguir=false;
        return true;
     }
     else return false;
    }
}
void Bolita::mover2(){

  borrar();
   if(dx>radio||dx<-radio){
    dx=0;}
   else dx=dx-2+random(5);
    if(dy>radio||dy<-radio){
     dy=0;}
   else dy=dy-2+random(5);
    //    dx=(((x>radio)||x<-radio)?0:(dy-2+random(8)));
//dy=(((y>radio)||y<-radio)?0:(dy-2+random(8)));
 x+=dx+1; y+=dy+3;

 if( x > TAMX -(radio%2)){
        //dx=-(2*dx);
        x=TAMX-radio%2-1;
        dx=-dx-5;
 }
 else if(x <=(radio%2)){ //dx=dx-3;
          x=0;
  dx=-dx;
         }
        if( y > TAMY - (radio%2)){
          dy=-dy;
          y=TAMY-radio%2;
        }
        else if(y<=radio%2){// dy=dy+3;
            y=0;
            dy=-dy;
        }

    pintar();
//rest(8);
}
void Bolita::mover3(){

  borrar();
   if(dx>radio||dx<-radio){
    dx=0;}
   else dx=dx-2+random(5);
    if(dy>radio||dy<-radio){
     dy=0;}
   else dy=dy-2+random(5);
 x+=dx+1; y+=dy+3;
 if( x > TAMX -(radio%2)){
        x=(radio%2);
        //dx=-dx-5;
 }
 else if(x <=(radio%2)){ //dx=dx-3;
          x=TAMX-(radio%2);
  //dx=-dx;
         }
        if( y > TAMY - (radio%2)){
         // dy=-dy;
          y=(radio%2);
        }
        else if(y<=radio%2){// dy=dy+3;
            y=TAMY - (radio%2);
           // dy=-dy;
        }

    pintar();
//rest(8);
}

